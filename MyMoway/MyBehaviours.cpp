// MyBehaviours.h : header file for the MyBehaviours Implementation
//

#include "stdafx.h"
#include "MyBehaviours.h"

int potenciaDerecha = 0;
int potenciaIzquierda = 0;
int reductorPotencia = 4;
bool ultimoDerecha = false;
bool voySiguiendoLinea = false;

// Functions implementation

void aliveBehaviour(CMoway *mymoway)
{
	//TODO1: IMPLEMENT ALIVE BEHAVIOUR:
	int luz;

	mymoway->ReadAmbientLightSensor(&luz);

	if (luz > 85) {
		mymoway->GoStraight(100 / reductorPotencia, CMoway::FORWARD, 5);
	}
	// END OF YOUR IMPLEMENTATION FOR ALIVE BEHAVIOUR
}

void fearBehaviour(CMoway *mymoway)
{
	//TODO2: IMPLEMENT FEAR BEHAVIOUR:
	int left, centerLeft, right, centerRight;

	mymoway->ChangeLEDState(CMoway::LED_FRONT, CMoway::ON);

	mymoway->ReadProximitySensors(&left, &centerLeft, &centerRight, &right);

	if (abs(centerLeft - centerRight) < 15) {
		int porcentaje = ((left * 100) / 255);
		potenciaDerecha = potenciaIzquierda = porcentaje;
	}
	else {
		if ((centerLeft != 0 && centerLeft > left) && (centerRight != 0 && centerRight > right)) {
			int porcentajeLeft = ((centerLeft * 100) / 255);
			int porcentajeRight = ((centerRight * 100) / 255);
			potenciaDerecha = porcentajeRight;
			potenciaIzquierda = porcentajeLeft;
		}

		if (left != 0 && left > centerLeft) {
			int porcentaje = ((left * 100) / 255);
			potenciaIzquierda = porcentaje;
			potenciaDerecha = 0;
		}
		if (right != 0 && right > centerRight) {
			int porcentaje = ((right * 100) / 255);
			potenciaDerecha = porcentaje;
			potenciaIzquierda = 0;
		}
	}
	mymoway->SetSpeed(potenciaIzquierda / reductorPotencia, potenciaDerecha / reductorPotencia, CMoway::BACKWARD, CMoway::BACKWARD, 5, 5);
	// END OF YOUR IMPLEMENTATION FOR FEAR BEHAVIOUR
}

void aggressionBehaviour(CMoway *mymoway)
{
	//TODO3: IMPLEMENT AGGRESSION BEHAVIOUR:
	int left, centerLeft, right, centerRight;

	mymoway->ChangeLEDState(CMoway::LED_FRONT, CMoway::ON);

	mymoway->ReadProximitySensors(&left, &centerLeft, &centerRight, &right);

	if (abs(centerLeft - centerRight) < 15) {
		int porcentaje = ((left * 100) / 255);
		potenciaDerecha = potenciaIzquierda = porcentaje;
	}
	else {
		if ((centerLeft != 0 && centerLeft > left) && (centerRight != 0 && centerRight > right)) {
			int porcentajeLeft = ((centerLeft * 100) / 255);
			int porcentajeRight = ((centerRight * 100) / 255);
			potenciaDerecha = porcentajeRight;
			potenciaIzquierda = porcentajeLeft;
		}

		if (left != 0 && left > centerLeft) {
			int porcentaje = ((left * 100) / 255);
			potenciaIzquierda = porcentaje;
			potenciaDerecha = 0;
		}
		if (right != 0 && right > centerRight) {
			int porcentaje = ((right * 100) / 255);
			potenciaDerecha = porcentaje;
			potenciaIzquierda = 0;
		}
	}
	mymoway->SetSpeed(potenciaDerecha / reductorPotencia, potenciaIzquierda / reductorPotencia, CMoway::FORWARD, CMoway::FORWARD, 5, 5);
	// END OF YOUR IMPLEMENTATION FOR AGGRESSION BEHAVIOUR
}

void loveBehaviour(CMoway *mymoway)
{
	//TODO4: IMPLEMENT LOVE BEHAVIOUR:
	int left, centerLeft, right, centerRight;

	mymoway->ChangeLEDState(CMoway::LED_FRONT, CMoway::ON);

	mymoway->ReadProximitySensors(&left, &centerLeft, &centerRight, &right);

	if (centerLeft == centerRight && centerLeft == 0) {
		potenciaDerecha = potenciaIzquierda = 0;
	}
	else {
		if ((centerLeft != 0 && centerLeft > left) && (centerRight != 0 && centerRight > right)) {
			int porcentajeLeft = ((centerLeft * 100) / 255);
			int porcentajeRight = ((centerRight * 100) / 255);
			potenciaDerecha = porcentajeRight;
			potenciaIzquierda = porcentajeLeft;
		}

		if (left != 0 && left >= centerLeft) {
			int porcentaje = ((left * 100) / 255);
			potenciaIzquierda = porcentaje;
		}
		if (right != 0 && right >= centerRight) {
			int porcentaje = ((right * 100) / 255);
			potenciaDerecha = porcentaje;
		}
	}

	if (centerLeft > 235 || centerRight > 235) {
		mymoway->MotorStop();
	}
	else {
		mymoway->SetSpeed((100 - potenciaIzquierda) / reductorPotencia, (100 - potenciaDerecha) / reductorPotencia, CMoway::FORWARD, CMoway::FORWARD, 5, 5);
	}
	// END OF YOUR IMPLEMENTATION FOR LOVE BEHAVIOUR
}

void exploreBehaviour(CMoway *mymoway)
{
	//TODO5: IMPLEMENT EXPLORE BEHAVIOUR:
	int left, centerLeft, right, centerRight;

	mymoway->ReadProximitySensors(&left, &centerLeft, &centerRight, &right);

	if (centerLeft == centerRight && centerLeft == 0) {
		potenciaDerecha = potenciaIzquierda = 0;
		mymoway->SetSpeed((100 - potenciaDerecha) / reductorPotencia, (100 - potenciaIzquierda) / reductorPotencia, CMoway::FORWARD, CMoway::FORWARD, 5, 5);
	}
	else {
		if (centerLeft > 240 || centerRight > 240)
		{
			if (centerRight > 240) {
				mymoway->SetSpeed(100, 0, CMoway::BACKWARD, CMoway::BACKWARD, 15, 15);
			}
			else {
				mymoway->SetSpeed(0, 100, CMoway::BACKWARD, CMoway::BACKWARD, 15, 15);
			}
			Sleep(15);
		}
		else {
			if ((centerLeft != 0 && centerLeft > left) && (centerRight != 0 && centerRight > right)) {
				int porcentajeLeft = ((centerLeft * 100) / 255);
				int porcentajeRight = ((centerRight * 100) / 255);
				potenciaDerecha = porcentajeRight;
				potenciaIzquierda = porcentajeLeft;
			}
			if (left != 0 && left > centerLeft) {
				int porcentaje = ((left * 100) / 255);
				potenciaIzquierda = porcentaje;
				potenciaDerecha = 0;
			}
			if (right != 0 && right > centerRight) {
				int porcentaje = ((right * 100) / 255);
				potenciaDerecha = porcentaje;
				potenciaIzquierda = 0;
			}
			mymoway->SetSpeed((100 - potenciaDerecha) / reductorPotencia, (100 - potenciaIzquierda) / reductorPotencia, CMoway::FORWARD, CMoway::FORWARD, 5, 5);
		}
	}
	// END OF YOUR IMPLEMENTATION FOR EXPLORE BEHAVIOUR
}

void obstacleBehaviour(CMoway *mymoway)
{
	//TODO6: IMPLEMENT OBSTACLE FOLLOWING BEHAVIOUR:
	int left, centerLeft, right, centerRight;

	mymoway->ChangeLEDState(CMoway::LED_FRONT, CMoway::ON);

	mymoway->ReadProximitySensors(&left, &centerLeft, &centerRight, &right);

	if (centerLeft > 190 || centerRight > 190)
	{
		if (centerRight > 190) {
			mymoway->SetSpeed(100, 0, CMoway::BACKWARD, CMoway::BACKWARD, 15, 15);
		}
		else {
			mymoway->SetSpeed(0, 100, CMoway::BACKWARD, CMoway::BACKWARD, 15, 15);
		}
		Sleep(15);
	}
	else {
		if (left > 0) {
			potenciaDerecha = ((left * 100) / 255);
		}
		else {
			potenciaDerecha = 0;
		}
		if (right > 0) {
			potenciaIzquierda = ((right * 100) / 255);
		}
		else {
			potenciaIzquierda = 0;
		}
		mymoway->SetSpeed((100 - potenciaDerecha) / reductorPotencia, (100 - potenciaIzquierda) / reductorPotencia, CMoway::FORWARD, CMoway::FORWARD, 5, 5);
	}
	// END OF YOUR IMPLEMENTATION FOR OBSTACLE FOLLOWING BEHAVIOUR
}


void lineBehaviour(CMoway *mymoway)
{
	//TODO7: IMPLEMENT LINE FOLLOWING BEHAVIOUR:
	int pleft, pcenterLeft, pright, pcenterRight;

	int left, right;

	bool ok = mymoway->ReadLineSensors(&left, &right);

	mymoway->ReadProximitySensors(&pleft, &pcenterLeft, &pcenterRight, &pright);

	if (pcenterLeft > 200 || pcenterRight > 200) {
		mymoway->SetSpeed(90, 90, CMoway::FORWARD, CMoway::BACKWARD, 8, 8);
		mymoway->SetSpeed(90, 90, CMoway::FORWARD, CMoway::BACKWARD, 8, 8);
		mymoway->SetSpeed(90, 90, CMoway::FORWARD, CMoway::BACKWARD, 8, 8);
		ultimoDerecha = !ultimoDerecha;
	}
	else
		if (left < 50 && right < 50) {
			potenciaDerecha = 100;
			potenciaIzquierda = 100;
			if (voySiguiendoLinea) {
				if (ultimoDerecha) {
					potenciaDerecha = 100;
					potenciaIzquierda = 50;
				}
				else {
					potenciaDerecha = 50;
					potenciaIzquierda = 100;
				}
			}
		}
		else {
			voySiguiendoLinea = true;
			if (left < 150 && right < 150) {
				potenciaDerecha = 100 - (((right * 100) / 255))*1.1;
				potenciaIzquierda = (100 - ((left * 100) / 255))*1.1;
			}
			else {
				if (left > 150) {
					potenciaDerecha = 0;
					potenciaIzquierda = 0;
					ultimoDerecha = false;
				}
				if (right > 150) {
					potenciaDerecha = 0;
					potenciaIzquierda = 0;
					ultimoDerecha = true;
				}
			}
		}
	mymoway->SetSpeed((100 - potenciaIzquierda) / (reductorPotencia * 6), (100 - potenciaDerecha) / (reductorPotencia * 6), CMoway::FORWARD, CMoway::FORWARD, 2, 2);
	// END OF YOUR IMPLEMENTATION FOR LINE FOLLOWING BEHAVIOUR
}
