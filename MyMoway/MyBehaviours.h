// MyBehaviours.h : header file for the MyBehaviours Implementation
//

/***** FILE NOT USED *****/
#include "stdafx.h"

// Functions exported

void aliveBehaviour(CMoway *mymoway);
void fearBehaviour(CMoway *mymoway);
void aggressionBehaviour(CMoway *mymoway);
void loveBehaviour(CMoway *mymoway);
void exploreBehaviour(CMoway *mymoway);
void obstacleBehaviour(CMoway *mymoway);
void lineBehaviour(CMoway *mymoway);

/***** FILE NOT USED *****/
